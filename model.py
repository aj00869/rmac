import pytorch_lightning as pl
from torch import nn
import torch
import torch.nn.functional as F
import dirtorch.nets as nets

import numpy as np
from torchsummary import summary


class FCSeries(nn.Module):
    """
    a set of FC layers separated by ReLU and/or Dropout
    """

    def __init__(self, d_input, layer_dims=[], dropout=0.0, relu_last=False):
        super(FCSeries, self).__init__()
        self.nlayers = len(layer_dims)
        self.all_dims = [d_input] + layer_dims
        self.dropout_layer = nn.Dropout(p=dropout)
        self.relu_last = relu_last
        self.fc_layers = nn.ModuleList()
        for i in range(self.nlayers):
            self.fc_layers.append(nn.Linear(self.all_dims[i], self.all_dims[i + 1]))
        self.init_weights()

    def init_weights(self):
        initrange = 0.1
        for i in range(self.nlayers):
            self.fc_layers[i].bias.data.zero_()
            self.fc_layers[i].weight.data.uniform_(-initrange, initrange)

    def forward(self, x):
        out = x
        for i in range(self.nlayers):
            out = self.fc_layers[i](out)
            if i < self.nlayers - 1:
                out = self.dropout_layer(F.relu(out))
            elif self.relu_last:  # last layer and relu_last=True
                out = F.relu(out)
        return out

    def extra_repr(self):
        out = '?x%d' % self.all_dims[0]
        if self.nlayers == 0:
            out += ' -> (identity) %s' % out
        else:
            for i in range(self.nlayers):
                out += ' -> ?x%d' % self.all_dims[i + 1]
        return out


class NTXentLoss2(nn.Module):
    """
    NTXentLoss version 2, now accepting an arbitrary number of k positives instead of just 2
    Example usage:
    loss1 = NTXentLoss2(5,3, batch_wise=True)  # loss1 operates on batch-wise data
    loss2 = NTXentLoss2(5,3, batch_wise=False)  # loss2 on sample-wise data
    x1 = torch.rand(15, 2)  # supposed x1 is batch-wise
    x2 = x1.reshape(3,5,-1).permute(1,0,2).reshape(15,2)  # x2 is sample-wise
    torch.allclose(loss1(x1), loss2(x2), 1e-3, 1e-3)
    """

    def __init__(self, batch_size, npos=2, temperature=1.0, batch_wise=False):
        """
        batch_size    number of image identities
        npos          number of augmented versions per image
        temperature   SimCLR temperature
        batch_wise    True if input data is batch-wise concatenated
          (a1,b1,c1,... a2,b2,c2...),
           else sample-wise concat (a1,a2,...b1,b2,...c1,c2...)
        """
        super(NTXentLoss2, self).__init__()
        self.batch_size = batch_size  # batch size
        self.npos = npos
        self.temperature = temperature
        self.batch_wise = batch_wise  # data is concat in batch or in sample
        self.register_buffer('mask', self.mask_correlated_samples(batch_size, npos))
        self.register_buffer('labels', torch.zeros(batch_size * npos).long())
        self.criterion = nn.CrossEntropyLoss(reduction="mean")
        self.similarity_f = nn.CosineSimilarity(dim=2)

    def mask_correlated_samples(self, batch_size, npos):
        """
        pre-create a mask of negative positions (for speed efficiency)
        this function just needs to be called once
        """
        mega_bsz = batch_size * npos
        mask = torch.ones((mega_bsz, mega_bsz), dtype=bool)
        if self.batch_wise:
            for i in range(mega_bsz):  # in each row ...
                for j in range(npos):  # there are npos positions to be masked out
                    mask[i, j * batch_size + i % batch_size] = 0
        else:
            for i in range(batch_size):
                mask[i * npos: (i + 1) * npos, i * npos:(i + 1) * npos] = 0
        return mask

    def forward(self, x):
        """
        :param x (batch_size * npos, D) mega batch input
        """
        mega_bsz = x.shape[0]
        bsz = mega_bsz // self.npos
        if mega_bsz == self.batch_size * self.npos:  # fast, using precomputed mask and labels
            mask, labels = self.mask, self.labels
        else:  # unexpected batch size, switch to slow version
            mask = self.mask_correlated_samples(bsz, self.npos).to(self.mask.device)
            labels = torch.zeros(mega_bsz).long().to(self.labels.device)
        sim = self.similarity_f(x.unsqueeze(1), x.unsqueeze(0)) / self.temperature
        negative_samples = sim[mask].reshape(mega_bsz, -1)
        if self.batch_wise:
            x_reshape = x.reshape(self.npos, bsz, -1).permute(1, 0, 2)
        else:
            x_reshape = x.reshape(bsz, self.npos, -1)
        positive_samples = self.similarity_f(x_reshape, x_reshape.mean(axis=1, keepdim=True)).reshape(
            mega_bsz, 1) / self.temperature
        logits = torch.cat((positive_samples, negative_samples), dim=1)

        loss = self.criterion(logits, labels)

        return loss


class ICNRMAC(pl.LightningModule):
    def __init__(self, hps):
        super().__init__()

        # Hyperparameters
        self.save_hyperparameters(hps)

        # Layers
        self.model = nets.create_model(pretrained="", arch='resnet18_rmac', out_dim=256, pooling='avg', gemp=3)
        # summary(self.model, (3, 224, 224), device='cpu')
        self.buffer = FCSeries(256, [128, 128], relu_last=False)
        # Losses
        self.loss = NTXentLoss2(self.hparams.bs, self.hparams.npos, temperature=1.0)

    def forward(self, x):
        x = self.model(x)
        # x = self.buffer(x)
        return x

    def any_step(self, imgs, batch_idx, stage):
        features = self(imgs.view(-1, 3, 224, 224))

        loss = self.loss(features)

        self.log(f'{stage}_loss', loss, on_step=True)

        return loss

    def training_step(self, batch, batch_idx):
        return self.any_step(batch, batch_idx, stage='train')

    def validation_step(self, batch, batch_idx):
        return self.any_step(batch, batch_idx, stage='val')

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.hparams.lr, weight_decay=5e-4)
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 1, gamma=0.75)
        return [optimizer], [scheduler]
