import os
import random

from torch.utils.data import Dataset
from torchvision import transforms
from PIL import Image
from torch.utils.data import DataLoader
from torchvision.utils import save_image
# from augmentation import Augly
import torch
from aug import Augly


class QueryImages(Dataset):
    def __init__(self, root):
        self.transforms = transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ])

        self.root = root

    def __getitem__(self, idx):
        img = Image.open(os.path.join(self.root, 'query_images', f'Q{idx:05d}.jpg'))
        return self.transforms(img)

    def __len__(self):
        return 50000


class ReferenceImages(Dataset):
    def __init__(self, root):
        self.transforms = transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ])

        self.root = root

    def __getitem__(self, idx):
        img = Image.open(os.path.join(self.root, 'reference_images', f'R{idx:06d}.jpg'))
        return self.transforms(img)

    def __len__(self):
        return 1000000


class TrainingImages(Dataset):
    def __init__(self, root, npos=2):
        self.transforms = transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ])
        self.augly = Augly()
        self.root = root
        self.npos = npos

    def __getitem__(self, idx):
        orig = Image.open(os.path.join(self.root, 'training_images', f'T{idx:06d}.jpg'))
        augs = [self.transforms(self.augly(orig)).unsqueeze(0) for i in range(self.npos - 1)]

        out = torch.cat([self.transforms(orig).unsqueeze(0)] + augs)
        return out

    def __len__(self):
        return 1000000
