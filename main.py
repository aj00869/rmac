import sys
import os
import os.path as osp
import pdb

import json
import numpy as np
import torch
import torch.nn.functional as F
from torch.utils.data import DataLoader, DistributedSampler

from dirtorch.utils.convenient import mkdir
from dirtorch.utils import common
from dirtorch.utils.common import tonumpy, matmul, pool
from dirtorch.utils.pytorch_loader import get_loader

import dirtorch.test_dir as test
import dirtorch.nets as nets
import dirtorch.datasets as datasets
import dirtorch.datasets.downloader as dl
from dirtorch.utils.pytorch_loader import get_loader
from isc_datasets import ReferenceImages, QueryImages
from tqdm import tqdm
from torchsummary import summary

from argparse import ArgumentParser


def load_model(path, iscuda):
    # checkpoint = common.load_checkpoint(path, iscuda)
    net = nets.create_model(pretrained="", arch='resnet18_rmac', out_dim=256, pooling='gem', gemp=3).to(device)

    # net.load_state_dict(checkpoint['state_dict'])
    # net.preprocess = checkpoint.get('preprocess', net.preprocess)

    return net.cuda()


if __name__ == '__main__':
    parser = ArgumentParser()

    # Trainer args
    parser.add_argument("-d", "--device", type=int)
    args = parser.parse_args()

    device = args.device

    net = nets.create_model(pretrained="", arch='resnet18_rmac', out_dim=256, pooling='gem', gemp=3).to(device)
    batch_size = 2
    dataset = TrainingImages('/mnt/tamatoa/scratch/alex/isc21')
    sampler = DistributedSampler(dataset, 3, device - 1)
    loader = DataLoader(dataset, batch_size=batch_size, num_workers=4, pin_memory=True)

    feats = []
    for batch in tqdm(loader, total=1 + (len(dataset) - 1) // batch_size):
        d = net(batch.to(device))
        feats.append(d.detach())

    np.save(f'r_feats_{device - 1}', np.array(feats))
