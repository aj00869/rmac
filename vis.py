import faiss
import numpy as np
import os
from PIL import Image, ImageOps
import pandas as pd


def concat_h(im1, im2, mode=Image.BICUBIC):
    r = im1.height / im2.height
    im2 = im2.resize((int(r * im2.width), im1.height), mode)
    dst = Image.new('RGB', (im1.width + im2.width, im1.height))
    dst.paste(im1, (0, 0))
    dst.paste(im2, (im1.width, 0))
    return dst


def concat_v(im1, im2, mode=Image.BICUBIC):
    r = im1.width / im2.width
    im2 = im2.resize((im1.width, int(r * im2.height)), mode)
    dst = Image.new('RGB', (im1.width, im1.height + im2.height))
    dst.paste(im1, (0, 0))
    dst.paste(im2, (0, im1.height))
    return dst


q = np.load('qf.npy')
r = np.load('rf.npy')

index = faiss.IndexFlatL2(256)
res = faiss.StandardGpuResources()
index = faiss.index_cpu_to_gpu(res, 0, index)

index.add(r)

gt = pd.read_csv(os.path.join(os.path.dirname(__file__), f'data/public_ground_truth.csv')).fillna(-1)
mask = [int(x[1:]) < 1600 if x != -1 else False for x in gt['reference_id']]
q_ids = gt[mask]

print(len(q_ids))

D, I = index.search(q[q_ids.index[:10]], 10)

full = None
acc = []
for i, row in enumerate(I):
    row_img = Image.open(os.path.join('/mnt/tamatoa/scratch/alex/isc21', 'query_images', f'Q{q_ids.index[i]:05d}.jpg')).resize((224, 224))
    for m, idx in enumerate(row):
        img = Image.open(os.path.join('/mnt/tamatoa/scratch/alex/isc21', 'reference_images', f'R{idx:06d}.jpg')).resize((224, 224))
        correct = int(q_ids['reference_id'].tolist()[i][1:]) == idx
        c = 'green' if correct else 'red'
        img = ImageOps.expand(img, border=10, fill=c)

        row_img = concat_h(row_img, img)

        if m == 0:
            acc.append(correct)

    if full is None:
        full = row_img
    else:
        full = concat_v(full, row_img)

print(sum(acc) / len(acc))
full.save('imgs/tst.jpg')
