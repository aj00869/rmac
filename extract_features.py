import pytorch_lightning as pl
from argparse import ArgumentParser
from torch.utils.data import DataLoader, DistributedSampler
import random
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
from model import ICNRMAC
from isc_datasets import QueryImages, ReferenceImages
import torch
import numpy as np
from tqdm import tqdm

device = 1
model = ICNRMAC.load_from_checkpoint('/home/alex/projects/dir/instance_retrieval/i2m4oul8/checkpoints/last.ckpt').to(device)

# Create dataloaders
ds_train = ReferenceImages('/mnt/tamatoa/scratch/alex/isc21')
train_dataloader = DataLoader(ds_train, batch_size=16,
                              num_workers=8)

features = []
for i, batch in tqdm(enumerate(train_dataloader), total=1000):
    f = model(batch.to(device))
    features.append(f.detach().cpu().numpy())
    if i > 1000:
        break

features = np.array(features)
features = np.reshape(features, (-1, 256))

np.save('rf', features)


# Create dataloaders
ds_train = QueryImages('/mnt/tamatoa/scratch/alex/isc21')
train_dataloader = DataLoader(ds_train, batch_size=16,
                              num_workers=8)

features = []
for i, batch in tqdm(enumerate(train_dataloader)):
    f = model(batch.to(device))
    features.append(f.detach().cpu().numpy())

features = np.array(features)
features = np.reshape(features, (-1, 256))

np.save('qf', features)

