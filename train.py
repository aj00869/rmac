from pytorch_lightning.loggers import WandbLogger
import pytorch_lightning as pl
from argparse import ArgumentParser
from torch.utils.data import DataLoader, DistributedSampler
import random
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
from model import ICNRMAC
from isc_datasets import TrainingImages
import torch
import numpy as np


def worker_init_fn(worker_id):
    torch_seed = torch.initial_seed()

    random.seed(torch_seed + worker_id)

    if torch_seed >= 2 ** 32:
        torch_seed = torch_seed % 2 ** 32
    np.random.seed(torch_seed + worker_id)


def main(args):
    # Init model
    model = ICNRMAC(args)
    # Init logger
    wandb_logger = WandbLogger(name=args.run_name, project='instance_retrieval', entity='cvssp2')

    # Create dataloaders
    ds_train = TrainingImages('/mnt/tamatoa/scratch/alex/isc21', args.npos)
    train_dataloader = DataLoader(ds_train, batch_size=args.bs,
                                  num_workers=args.num_workers,
                                  worker_init_fn=worker_init_fn)

    # Train!
    checkpoint_callback = ModelCheckpoint(
        verbose=True,
        save_last=True,
        every_n_train_steps=100
    )

    trainer = pl.Trainer(gpus=args.gpus, logger=wandb_logger,
                         log_every_n_steps=1,
                         callbacks=[checkpoint_callback],
                         accelerator='dp'
                         )
    trainer.fit(model,
                train_dataloader=train_dataloader
                )


if __name__ == '__main__':
    parser = ArgumentParser()

    # Model args
    parser.add_argument("--lr", default=1e-4, type=float, required=False)
    parser.add_argument("--bs", default=32, type=int, required=False)
    parser.add_argument("--npos", default=4, type=int, required=False)
    parser.add_argument("--temp", default=1.0, type=float, required=False)

    # Trainer args
    parser.add_argument("--num_workers", default=4, type=int, required=False)
    parser.add_argument("--gpus", default=2, type=int, nargs='+', required=False)
    parser.add_argument("--run_name", default='unnamed', type=str, required=False)

    args = parser.parse_args()

    main(args)
